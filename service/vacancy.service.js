const { VacancyRepository } = require('../repositories/vacancy.repository');

const vacancyRepository = new VacancyRepository();

class VacancyService {
  async getVacancy() {
    const vacancy = (await vacancyRepository.getVacancy()).rows;

    if (!vacancy || !Array.isArray(vacancy)) {
      throw new Error('Something went wrong. Data not received.');
    }

    return { vacancy };
  }

  async createVacancy(vacancy) {
    const date_update = new Date();
    const params = [
      vacancy.name,
      vacancy.city,
      vacancy.experience_id,
      vacancy.salary_from,
      vacancy.salary_to,
      vacancy.description,
      vacancy.active,
      date_update,
    ];

    const vacancyId = (await vacancyRepository.createVacancy(params)).rows[0]
      ?.id;

    if (!vacancyId) {
      throw new Error('Something went wrong. Data not saved.');
    }

    return { vacancyId };
  }

  async updateVacancy(vacancy) {
    const date_update = new Date();
    const params = [
      vacancy.id,
      vacancy.name,
      vacancy.city,
      vacancy.experience_id,
      vacancy.salary_from,
      vacancy.salary_to,
      vacancy.description,
      vacancy.active,
      date_update,
    ];

    const vacancyId = (await vacancyRepository.updateVacancy(params)).rows[0]
      ?.id;

    if (!vacancyId) {
      throw new Error('Something went wrong. Data not updated.');
    }

    return { vacancyId };
  }

  async deleteVacancy(vacancy) {
    const vacancyId = (await vacancyRepository.deleteVacancy([vacancy.id]))
      .rows[0]?.id;

    if (!vacancyId) {
      throw new Error('Something went wrong. Data not deleted.');
    }

    return { vacancyId };
  }
}

module.exports = {
  VacancyService,
};
