const { CvRepository } = require('../repositories/cv.repository');

const cvRepository = new CvRepository();

class CvService {
  async getCv() {
    const cv = (await cvRepository.getCv()).rows;

    if (!cv || !Array.isArray(cv)) {
      throw new Error('Something went wrong. Data not received.');
    }

    return { cv };
  }

  async createCv(cv) {
    const date_update = new Date();
    const params = [
      cv.full_name,
      cv.professional_grade_id,
      cv.specialization_id,
      cv.experience,
      cv.english_grade_id,
      JSON.stringify(cv.technologies),
      JSON.stringify(cv.projects),
      cv.active,
      date_update,
    ];

    const cvId = (await cvRepository.createCv(params)).rows[0]?.id;

    if (!cvId) {
      throw new Error('Something went wrong. Data not saved.');
    }

    return { cvId };
  }

  async updateCv(cv) {
    const date_update = new Date();
    const params = [
      cv.id,
      cv.full_name,
      cv.professional_grade_id,
      cv.specialization_id,
      cv.experience,
      cv.english_grade_id,
      JSON.stringify(cv.technologies),
      JSON.stringify(cv.projects),
      cv.active,
      date_update,
    ];

    const cvId = (await cvRepository.updateCv(params)).rows[0]?.id;

    if (!cvId) {
      throw new Error('Something went wrong. Data not updated.');
    }

    return { cvId };
  }

  async deleteCv(cv) {
    const cvId = (await cvRepository.deleteCv([cv.id])).rows[0]?.id;

    if (!cvId) {
      throw new Error('Something went wrong. Data not deleted.');
    }

    return { cvId };
  }
}

module.exports = {
  CvService,
};
