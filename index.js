require('dotenv').config();

const express = require('express');
const cors = require('cors');
const geo = require('geo');
const Sentry = require('@sentry/node');

geo.init();

const contactRoutes = require('./routes/contact/contact.routes');
const cvRoutes = require('./routes/cv/cv.routes');
const vacancyRoutes = require('./routes/vacancy/vacancy.routes');

const app = express();

const port = process.env.PORT || '3001';
const SENTRY_DSN =
  process.env.SENTRY_DSN ||
  'https://e2650849af3e4e5f9a276ed386d6d123@o1272335.ingest.sentry.io/6465864';

Sentry.init({
  dsn: SENTRY_DSN,
  // Adjust this value in production, or use tracesSampler for greater control
  tracesSampleRate: 1.0,
  // CI/CD is working absolutely working!!!:)
  // Note: if you want to override the automatic release value, do not set a
  // `release` value here - use the environment variable `SENTRY_RELEASE`, so
  // that it will also get attached to your source maps
});

app.use(express.json({ limit: '25mb' }));
app.use(express.urlencoded({ extended: true, limit: '25mb' }));
app.use(cors());

app.use('/api/contact', contactRoutes);
app.use('/api/cv', cvRoutes);
app.use('/api/vacancy', vacancyRoutes);

app.listen(port, () => {
  console.log(`Listening to requests on ${port}`);
});
