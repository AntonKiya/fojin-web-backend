const { pool } = require('../connectors/pg-connector');

class CvRepository {
  getCv() {
    return pool.query(`
      SELECT 
        cv.id, 
        full_name, 
        professional_grade.value AS "professional_grade", 
        specialization.value AS "specialization", 
        experience, 
        english_grade.value AS "english_grade", 
        (
          SELECT 
            json_agg(
              json_build_object(
                'id', technologies.id, 'value', technologies.value
              )
            ) 
          FROM 
            technologies 
          WHERE 
            technologies.id in (
              select 
                jsonb_array_elements(cv.technologies):: integer
            )
        ) AS "stack", 
        (
          SELECT 
            json_agg(
              json_build_object(
                'id', projects.id, 'value', projects.value
              )
            ) 
          FROM 
            projects 
          WHERE 
            projects.id in (
              select 
                jsonb_array_elements(cv.projects):: integer
            )
        ) AS "projects", 
        active, 
        date_create, 
        date_update 
      FROM 
        cv 
        INNER JOIN professional_grade ON cv.professional_grade_id = professional_grade.id 
        INNER JOIN specialization ON cv.specialization_id = specialization.id 
        INNER JOIN english_grade ON cv.english_grade_id = english_grade.id;
    `);
  }

  createCv(cvParams) {
    return pool.query(`
      INSERT INTO cv (
        full_name, professional_grade_id, 
        specialization_id, experience, english_grade_id, 
        technologies, projects, active, date_update
      ) 
      VALUES 
        ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id;`,
      cvParams,
    );
  }

  updateCv(cvParams) {
    return pool.query(`
      UPDATE 
        cv 
      SET 
        full_name = $2, 
        professional_grade_id = $3, 
        specialization_id = $4, 
        experience = $5, 
        english_grade_id = $6, 
        technologies = $7, 
        projects = $8, 
        active = $9, 
        date_update = $10 
      WHERE 
        cv.id = $1 RETURNING id;`,
      cvParams,
    );
  }

  deleteCv(cvParams) {
    return pool.query(`
      DELETE FROM 
        cv 
      WHERE 
        cv.id = $1 RETURNING id;`,
      cvParams,
    );
  }
}

module.exports = {
  CvRepository,
};
