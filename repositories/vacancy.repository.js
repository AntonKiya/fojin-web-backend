const { pool } = require('../connectors/pg-connector');

class VacancyRepository {
  getVacancy() {
    return pool.query(`
      SELECT 
        vacancies.id, 
        name, 
        city, 
        experience.value AS "experience", 
        salary_from, 
        salary_to, 
        description, 
        active, 
        date_create, 
        date_update 
      FROM 
        vacancies 
        INNER JOIN experience ON experience_id = experience.id;
    `);
  }

  createVacancy(vacancyParams) {
    return pool.query(`
      INSERT INTO vacancies (
        name, city, experience_id, salary_from, 
        salary_to, description, active, date_update
      ) 
      VALUES 
        ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;`,
      vacancyParams,
    );
  }

  updateVacancy(vacancyParams) {
    return pool.query(`
      UPDATE 
        vacancies 
      SET 
        name = $2, 
        city = $3, 
        experience_id = $4, 
        salary_from = $5, 
        salary_to = $6, 
        description = $7, 
        active = $8, 
        date_update = $9 
      WHERE 
        id = $1 RETURNING id;`,
      vacancyParams
    );
  }

  deleteVacancy(vacancyParams) {
    return pool.query(`
      DELETE FROM 
        vacancies 
      WHERE 
        vacancies.id = $1 RETURNING id;`,
      vacancyParams,
    );
  }
}

module.exports = {
  VacancyRepository,
};
