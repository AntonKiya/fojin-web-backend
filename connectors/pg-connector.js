const { Pool } = require('pg');

const pg_config = {
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST,
  port: process.env.PG_PORT,
  database: process.env.PG_DB_NAME,
};

const pool = new Pool(pg_config);

pool.connect((err) => {
  if (err) {
    console.error('Error connection', err.stack);
  } else {
    console.log(`Successful connection to "${process.env.PG_DB_NAME}"`);
  }
});

module.exports = {
  pool,
};
