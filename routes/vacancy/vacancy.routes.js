const express = require('express');
const router = express.Router();

const parseMultipartForm = require('../../middleware/multipartFormParser');
const {
  getVacancy,
  createVacancy,
  updateVacancy,
  deleteVacancy,
} = require('../../controllers/vacancy.controller');
const {
  vacancyCreateDto,
  vacancyUpdateDto,
  vacancyDeleteDto,
} = require('./vacancy.dto');

router.use(parseMultipartForm);

router.get('/list', getVacancy);
router.post('/', vacancyCreateDto, createVacancy);
router.patch('/:id', vacancyUpdateDto, updateVacancy);
router.delete('/:id', vacancyDeleteDto, deleteVacancy);

module.exports = router;
