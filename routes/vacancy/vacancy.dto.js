const { body, param } = require('express-validator');

const vacancyCreateDto = [
  body('vacancy.name', 'name must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.city', 'city must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.experience_id', 'experience_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.salary_from', 'salary_from must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.salary_to', 'salary_to must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.description', 'description must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.active', 'active must be a boolean')
    .exists()
    .isBoolean()
    .not()
    .isString(),
];

const vacancyUpdateDto = [
  param('id', 'id must be a number')
    .toInt()
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.name', 'name must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.city', 'city must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.experience_id', 'experience_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.salary_from', 'salary_from must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.salary_to', 'salary_to must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('vacancy.description', 'description must be a string')
    .exists()
    .isString()
    .not()
    .isNumeric(),
  body('vacancy.active', 'active must be a boolean')
    .exists()
    .isBoolean()
    .not()
    .isString(),
];

const vacancyDeleteDto = [
  param('id', 'id must be a number')
    .toInt()
    .exists()
    .isNumeric()
    .not()
    .isString(),
];

module.exports = {
  vacancyCreateDto,
  vacancyUpdateDto,
  vacancyDeleteDto,
};
