const express = require('express');
const router = express.Router();

const parseMultipartForm = require('../middleware/multipartFormParser');
const { sendContact } = require('../controllers/new_contact');

router.use(parseMultipartForm);

router.post('/contact', sendContact);

module.exports = router;
