const { body, param } = require('express-validator');

const cvUpdateDto = [
  param('id', 'id must be a number')
    .toInt()
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.full_name', 'full_name must be a string').exists().isString(),
  body('cv.professional_grade_id', 'professional_grade_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.specialization_id', 'specialization_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.experience', 'experience must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.english_grade_id', 'english_grade_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.active', 'active must be a boolean')
    .exists()
    .isBoolean()
    .not()
    .isString(),
  body('cv.technologies', 'technologies must be an array')
    .exists()
    .isArray()
    .not()
    .isObject(),
  body('cv.technologies.*', 'each technology must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.projects', 'projects must be an array')
    .exists()
    .isArray()
    .not()
    .isObject(),
  body('cv.projects.*', 'each project must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
];

const cvCreateDto = [
  body('cv.full_name', 'full_name must be a string').exists().isString(),
  body('cv.professional_grade_id', 'professional_grade_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.specialization_id', 'specialization_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.experience', 'experience must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.english_grade_id', 'english_grade_id must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.active', 'active must be a boolean')
    .exists()
    .isBoolean()
    .not()
    .isString(),
  body('cv.technologies', 'technologies must be an array')
    .exists()
    .isArray()
    .not()
    .isObject(),
  body('cv.technologies.*', 'each technology must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
  body('cv.projects', 'projects must be an array')
    .exists()
    .isArray()
    .not()
    .isObject(),
  body('cv.projects.*', 'each project must be a number')
    .exists()
    .isNumeric()
    .not()
    .isString(),
];

const cvDeleteDto = [
  param('id', 'id must be a number')
    .toInt()
    .exists()
    .isNumeric()
    .not()
    .isString(),
];

module.exports = {
  cvCreateDto,
  cvUpdateDto,
  cvDeleteDto,
};
