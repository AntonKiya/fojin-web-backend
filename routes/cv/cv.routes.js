const express = require('express');
const router = express.Router();

const parseMultipartForm = require('../../middleware/multipartFormParser');
const {
  getCv,
  createCv,
  updateCv,
  deleteCv,
} = require('../../controllers/cv.controller');
const { cvCreateDto, cvUpdateDto, cvDeleteDto } = require('./cv.dto');

router.use(parseMultipartForm);

router.get('/list', getCv);
router.post('/', cvCreateDto, createCv);
router.patch('/:id', cvUpdateDto, updateCv);
router.delete('/:id', cvDeleteDto, deleteCv);

module.exports = router;
