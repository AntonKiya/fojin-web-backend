require('dotenv').config();

/* modules */
const fs = require('fs');
const { StatusCodes } = require('http-status-codes');
const { captureException } = require('@sentry/node');

const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: 'smtp.sendgrid.net',
  port: 587,
  auth: {
    user: 'apikey',
    pass: process.env.SENDGRID_API_KEY,
  },
});

async function sendContact(req, res) {
  try {
    const { name, email, about } = req.body;
    const file = req.files.attachment;

    const mailData = {
      from: process.env.GMAIL_EMAIL_FROM, // verified sender email
      to: process.env.GMAIL_EMAIL_TO, // recipient email
      bcc: process.env.GMAIL_EMAIL_BCC,
      subject: `Fojin Landing page. Message from ${name}`,
      text: `${about} | Email: ${email}`,
      html: `<div>${about}</div><p>Email:
        ${email}</p>`,
    };

    if (file) {
      mailData.attachments = [
        {
          filename: file.originalFilename,
          content: fs.createReadStream(file.filepath),
        },
      ];
    }

    const result = await new Promise((resolve, reject) => {
      transporter.sendMail(mailData, (err) => {
        if (err) {
          reject(err);
        }
        return resolve('success');
      });
    });

    if (result !== 'success') {
      throw new Error('email_error');
    }
  } catch (err) {
    captureException(err);
  } finally {
    res.status(StatusCodes.OK).send('Email successfully sent!');
  }
}

module.exports = {
  sendContact,
};
