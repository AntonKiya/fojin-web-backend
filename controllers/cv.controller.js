const { validationResult } = require('express-validator');
const { CvService } = require('../service/cv.service');

const cvService = new CvService();

async function getCv(req, res) {
  try {
    const cvList = await cvService.getCv();

    res.send(cvList);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function createCv(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors.array(),
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const cvId = await cvService.createCv(req.body.cv);

    res.send(cvId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function updateCv(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors.array(),
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const cvId = await cvService.updateCv({ ...req.params, ...req.body.cv });

    return res.send(cvId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function deleteCv(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors.array(),
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const cvId = await cvService.deleteCv({ ...req.params });

    return res.send(cvId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

module.exports = {
  getCv,
  createCv,
  updateCv,
  deleteCv,
};
