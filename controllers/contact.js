// DON'T USE THIS FILE. It is depricated

require("dotenv").config();

/* modules */
const fs = require("fs");
const { StatusCodes } = require("http-status-codes");
const { captureException } = require("@sentry/node");

const nodemailer = require("nodemailer");
const { google } = require("googleapis");

const OAuth2 = google.auth.OAuth2;

const createOAuthTransport = async () => {
  const OAuth2Client = new OAuth2(
    process.env.OAUTH_CLIENT_ID,
    process.env.OAUTH_CLIENT_SECRET
  );

  OAuth2Client.setCredentials({
    refresh_token: process.env.OAUTH_REFRESH_TOKEN,
  });

  const accessToken = OAuth2Client.getAccessToken();

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: process.env.GMAIL_EMAIL_FROM,
      clientId: process.env.OAUTH_CLIENT_ID,
      clientSecret: process.env.OAUTH_CLIENT_SECRET,
      refreshToken: process.env.OAUTH_REFRESH_TOKEN,
      accessToken,
    },
  });

  return transporter;
};

async function sendContact(req, res) {
  try {
    const { name, email, about } = req.body;
    const file = req.files.attachment;

    const mailData = {
      from: process.env.GMAIL_EMAIL_FROM,
      to: process.env.GMAIL_EMAIL_TO,
      subject: `Fojin Landing page. Message from ${name}`,
      text: `${about} | Email: ${email}`,
      html: `<div>${about}</div><p>Email:
        ${email}</p>`,
    };

    if (file) {
      mailData.attachments = [
        {
          filename: file.originalFilename,
          content: fs.createReadStream(file.filepath),
        },
      ];
    }

    const transporter = await createOAuthTransport();

    const result = await new Promise((resolve, reject) => {
      transporter.sendMail(mailData, (err) => {
        if (err) {
          reject(err);
        }
        return resolve("success");
      });
    });

    if (result !== "success") {
      throw new Error("email_error");
    }
  } catch (err) {
    captureException(err);
  } finally {
    res.status(StatusCodes.OK).send("Email successfully sent to recipient!");
  }
}

module.exports = {
  sendContact,
};
