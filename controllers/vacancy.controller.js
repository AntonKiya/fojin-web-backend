const { validationResult } = require('express-validator');
const { VacancyService } = require('../service/vacancy.service');

const vacancyService = new VacancyService();

async function getVacancy(req, res) {
  try {
    const vacancy = await vacancyService.getVacancy();

    res.send(vacancy);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function createVacancy(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors.array(),
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const vacancyId = await vacancyService.createVacancy(req.body.vacancy);

    res.send(vacancyId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function updateVacancy(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors,
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const vacancyId = await vacancyService.updateVacancy({
      ...req.params,
      ...req.body.vacancy,
    });

    return res.send(vacancyId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

async function deleteVacancy(req, res) {
  try {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        validationErrors: validationErrors.array(),
        message: 'Something went wrong. Check the correctness of the data',
      });
    }

    const vacancyId = await vacancyService.deleteVacancy({ ...req.params });

    return res.send(vacancyId);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
}

module.exports = {
  getVacancy,
  createVacancy,
  updateVacancy,
  deleteVacancy,
};
